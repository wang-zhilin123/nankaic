#ifndef BOSS_H
#define BOSS_H
#include<QPixmap>
#include"bossapple.h"
#include <cstdlib> // 包含 rand() 和 srand()
#include <ctime>   // 包含 time()
class boss
{
public:
    boss();
    //发射函数
    void shoot();
    //更改位置
    void updatePosition();

    //设置图片
    QPixmap pix;
    //坐标
    int b_x;
    int b_y;
    //矩形边框
    QRect b_rect;

    //发射间隔
    int b_recorder;
    //状态
    bool b_free;
    //血量
    int blood;
    //弹夹
    bossapple bgun[30];
    //随机数收纳
    int ran;

};

#endif // BOSS_H
