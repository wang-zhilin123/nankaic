#ifndef PLAYSCENE_H
#define PLAYSCENE_H
#include"gong.h"
#include <QWidget>
#include"piaofuling.h"
#include<QTimer>
#include"apple.h"
#include"bomb.h"
#include<QMediaPlayer>
#include <QAudioOutput>
#include <QMouseEvent>
#include"mypushbutton.h"
#include"boss.h"
#include"bossapple.h"
class playScene : public QWidget
{
    Q_OBJECT
public:
    playScene(QWidget *parent = nullptr);
    ~playScene();

    void paintEvent(QPaintEvent *);//绘制背景

    void intScene();//初始化图标

    void playGame1();//启动游戏

    void playGame2();

    void updatePosition1();//更新游戏中所有元素的坐标

    void updatePosition2();

    void colisionDetection();//碰撞检测

    void mouseMoveEvent(QMouseEvent *);//重写鼠标移动事件

    void piaoToScene();//敌人出场

    void bossToScene(int);//boss出场

    bomb Bomb[50];//爆炸数组

    QTimer *timer1;//全场主定时器

    QTimer *timer2;//全场副定时器

    gong Gong;//gong

    piaofuling Piao[50];//敌人数组

    int p_recorder;//敌人出场间隔

    boss Boss;//生成boss

    QPixmap gameOverlosefu;//游戏结束fu

    QPixmap gameOverlosetao;//游戏结束tao

    int fin;//判断Gong是否胜利

    QPixmap gameWinfu;//游戏胜利芙

    QPixmap gameWintao;//游戏胜利桃


signals:
};

#endif // PLAYSCENE_H
