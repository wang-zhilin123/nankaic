#ifndef SHOU_H
#define SHOU_H
#include<QRect>
#include"apple.h"
class shou
{
public:
    shou();
    //血量
    int shouBlood;
    //状态
    bool s_free;
    //设置图片
    QPixmap pix;
    //坐标
    int s_x;
    int s_y;
    //矩形边框
    QRect s_rect;
    //弹夹
    apple s_apple[30];
    //发射间隔
    int s_recorder;
    //发射子弹
    void shoot();
    //设置坐标
    void setPosition();

};

#endif // SHOU_H
