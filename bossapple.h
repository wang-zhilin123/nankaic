#ifndef BOSSAPPLE_H
#define BOSSAPPLE_H
#include<QPixmap>
class bossapple
{
public:
    bossapple();
    //坐标
    void updateposition();

    //图片资源
    QPixmap pix;
    //坐标
    int ba_x;
    int ba_y;
    //速度
    int ba_speed;
    //是否闲置
    bool ba_free;
    //边框
    QRect ba_rect;

};

#endif // BOSSAPPLE_H
