#include "piaofuling.h"
#include<QPixmap>
piaofuling::piaofuling() {
    pix.load(":/ScreenShot/piaofuling.png");

    p_x = -pix.width();
    p_y = -pix.height();

    p_free = true;

    p_speed = 10;

    p_rect.setSize(QSize(pix.width(),pix.height()));
    p_rect.moveTo(p_x,p_y);


}
void piaofuling::updatePosition()
{
    if(p_free)
    {
        return;
    }
    p_y+=p_speed;
    p_rect.moveTo(p_x,p_y);

    if(p_y>=800 + pix.height()+pix.height())
    {
        p_free = true;
    }
}
