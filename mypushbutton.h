#ifndef MYPUSHBUTTON_H
#define MYPUSHBUTTON_H

#include <QPushButton>

class MyPushButton : public QPushButton
{
    Q_OBJECT
public:
    MyPushButton(QString normallImg,QString pressImg = "");

    //来储存传入图片地址
    QString normalImgPath;
    QString pressImgpath;

    //做弹起特效
    void zoom1();
    void zoom2();
    //重写按钮 按下和 释放事件
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
signals:
};

#endif // MYPUSHBUTTON_H
