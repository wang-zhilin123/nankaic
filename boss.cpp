#include "boss.h"
#include <cstdlib> // 包含 rand() 和 srand()
#include <ctime>   // 包含 time()
#include<QDebug>
boss::boss() {

    b_free = true;
    b_recorder = 0;
}
void boss::shoot()
{
    b_recorder++;
    if(b_recorder < 20)
    {
        return;
    }
    b_recorder = 0;
    for(int i = 0;i<30;i++)
    {
        if(bgun[i].ba_free)
        {
            bgun[i].ba_free = false;
            bgun[i].ba_x = b_x + pix.width()*0.5;
            bgun[i].ba_y = b_y + pix.height();
            break;
       }
    }
}

void boss::updatePosition()
{
    srand((unsigned int)time(NULL));
    ran = (rand() % 2) + 1;
    if(ran == 1)
    {
        b_x+=8;
        if(b_x >= 1320-pix.width())
        {
            b_x = 1320-pix.width();
        }
        b_rect.moveTo(b_x,b_y);

    }
    else
    {
        b_x-=8;
        if(b_x <= pix.width())
        {
            b_x = pix.width();
        }
        b_rect.moveTo(b_x,b_y);

    }

}
