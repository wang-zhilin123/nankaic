#ifndef CHOOSESCENE_H
#define CHOOSESCENE_H
#include"playscene.h"
#include <QWidget>
#include<playtwo.h>
class chooseScene : public QWidget
{
    Q_OBJECT
public:
    explicit chooseScene(QWidget *parent = nullptr);
    MyPushButton * choosebtn1;
    MyPushButton * choosebtn2;
    void intScene();
    void paintEvent(QPaintEvent *event);
    playScene *play;
    playtwo *play2;
signals:
};

#endif // CHOOSESCENE_H
