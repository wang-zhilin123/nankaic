#include "bomb.h"
#include<QStringList>
bomb::bomb() {

    QStringList res = {
        ":/ScreenShot/bomb001.png",
        ":/ScreenShot/bomb002.png",
        ":/ScreenShot/bomb003.png",
        ":/ScreenShot/bomb004.png",
        ":/ScreenShot/bomb005.png",
        ":/ScreenShot/bomb006.png",
        ":/ScreenShot/bomb007.png",
        ":/ScreenShot/bomb008.png"
    };
    for(int i = 0;i<8;i++)
    {
        QPixmap pixmap(res[i]);
        m_pixarr.push_back(pixmap);
    }

    //坐标
    m_x = 0;
    m_y = 0;
    //空闲状态
    m_free = true;
    //当前图片下标
    m_index = 0;
    //播放爆炸间隔记录
    m_Recorder = 0;
}
void bomb::updateInfo()
{
    //空闲状态下，不引爆
    if(m_free)
    {
        return;
    }
    m_Recorder++;
    //爆炸时间未达到爆炸间隔，不切图
    if(m_Recorder<20)
    {
        return;
    }
    //重置记录
    m_Recorder = 0;
    //切换爆炸播放的图片下标
    m_index++;
    if(m_index >= 7)
    {
        m_index = 0;
        m_free = true;
    }

}
