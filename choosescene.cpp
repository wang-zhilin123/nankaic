#include "choosescene.h"
#include<QPainter>
#include"mypushbutton.h"
#include<QDebug>
#include<QTimer>
#include<QLabel>
#include"playscene.h"
chooseScene::chooseScene(QWidget *parent)
    : QWidget{parent}
{
    intScene();
    choosebtn1 = new MyPushButton(":/ScreenShot/fufu1.jpg",":/ScreenShot/fufu2.jpg");
    choosebtn1->setParent(this);
    choosebtn1->move(300,400);
    choosebtn2 = new MyPushButton(":/ScreenShot/taoc1.png",":/ScreenShot/taofin.png");
    choosebtn2->setParent(this);
    choosebtn2->move(800,400);
    play = new playScene;
    play2 = new  playtwo;
    connect(choosebtn1,&MyPushButton::released,[=](){
        play->show();
        this->close();
    });
    connect(choosebtn2,&MyPushButton::released,[=](){
        play2->show();
        this->close();
    });

    connect(choosebtn1,&MyPushButton::clicked,[=](){
        qDebug()<<"化身黑虎阿芙";
        choosebtn1->zoom1();
        choosebtn1->zoom2();
    });
    connect(choosebtn2,&MyPushButton::clicked,[=](){
        qDebug()<<"化身第一火A";
        choosebtn1->zoom1();
        choosebtn2->zoom2();
    });
}

void chooseScene::intScene()
{
    //配置选择关卡页面
    this->setFixedSize(1320,800);
    this->setWindowIcon(QIcon(":/ScreenShot/win!.jpg"));
    this->setWindowTitle("芙桃大战");
}
void chooseScene::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/ScreenShot/choose.png");
    painter.drawPixmap(0,0,this->width(),this->height(),pix);
}
