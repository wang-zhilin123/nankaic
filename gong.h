#ifndef GONG_H
#define GONG_H
#include<QPixmap>
#include"apple.h"
class gong
{
public:
    gong();
    //发射苹果
    void shoot();
    //设置飞机位置
    void setPosition(int x,int y);
    //飞机图片
    QPixmap pix;
    //飞机坐标
    int g_x;
    int g_y;
    //矩形边框
    QRect g_rect;
    //弹匣
    apple g_apple[30];
    //发射记录
    int g_recorder;
    //飞机状态
    bool g_free;

};

#endif // GONG_H
