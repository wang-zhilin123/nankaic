#ifndef APPLE_H
#define APPLE_H
#include<QPixmap>
#include<QRect>
class apple
{
public:
    apple();
    //苹果对象
    QPixmap pix;
    //苹果坐标
    int a_x;
    int a_y;
    //更新苹果坐标
    void updatePosition();
    //苹果移动速度
    int a_speed;
    //苹果是否闲置
    bool a_free;
    //苹果矩形边框（检测碰撞）
    QRect a_rect;
    //发射间隔
    int a_recorder;


};

#endif // APPLE_H
