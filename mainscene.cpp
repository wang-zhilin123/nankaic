#include "mainscene.h"
#include "ui_mainscene.h"
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QVBoxLayout>
#include<mypushbutton.h>
#include<QPainter>
#include<QTimer>
#include <cstdlib> // 包含 rand() 和 srand()
#include <ctime>   // 包含 time()
mainScene::mainScene(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::mainScene)
{
    ui->setupUi(this);
    srand((unsigned int)time(NULL));
    an = rand() % 3;
    //配置登录页面
    intScene();
    musicplay();
    buttonSet1(":/ScreenShot/truestart.png");
    buttonSet2(":/ScreenShot/exit.png");
    choosescene = new chooseScene;
}

mainScene::~mainScene()
{
    delete ui;
}
void mainScene::intScene()
{
    //配置登录页面
    this->setFixedSize(1320,800);
    this->setWindowIcon(QIcon(":/ScreenShot/win!.jpg"));
    this->setWindowTitle("芙桃大战");
}
void mainScene::musicplay()
{
    mu = new QMediaPlayer;
    audioOutput = new QAudioOutput;
    mu->setAudioOutput(audioOutput);
    mu->setSource(QUrl::fromLocalFile("D:/CloudMusic/hope.mp3"));
    audioOutput->setVolume(50);
    // mu->setLoops(QSoundEffect::Infinite);  //设置无限循环
    mu->play();

    // mu = new QMediaPlayer;
    // mu->setSource(QUrl::fromLocalFile("D:/CloudMusic/hope.mp3"));
    // mu->setLoops(QMediaPlayer::Infinite);
    // mu->play();
}
void mainScene::videoPlay()
{

    // 创建一个QMediaPlayer实例
    player = new QMediaPlayer(this);

    // 创建一个QVideoWidget实例并添加到布局中
    videoWidget = new QVideoWidget(this);
    videoWidget->show();
    videoWidget->setFixedSize(this->width(),this->height());
    videoWidget->setAspectRatioMode(Qt::IgnoreAspectRatio);
    switch (an) {
    case 0:
        // 设置视频源（请替换为你的视频文件路径）
        player->setSource(QUrl::fromLocalFile("D:/Qt/bgbgfir.mp4"));
        break;
    case 1:
        player->setSource(QUrl::fromLocalFile("D:/Qt/go!.mp4"));
        break;
    case 2:
        player->setSource(QUrl::fromLocalFile("D:/Qt/bgbgend.mp4"));
        break;
    }


    // 将QMediaPlayer与QVideoWidget连接
    player->setVideoOutput(videoWidget);

    // 播放视频
    player->play();

    QTimer::singleShot(8000,this,[=](){
        choosescene->show();
        videoWidget->close();
        this->close();

    });

}
void mainScene::buttonSet1(QString str1,QString str2)
{
    //配置开始按钮
    MyPushButton * btn = new MyPushButton(str1,str2);
    btn->setParent(this);
    btn->setFixedSize(429,70);
    btn->move(250,650);
    connect(btn,&MyPushButton::clicked,this,&mainScene::videoPlay);
}
void mainScene::buttonSet2(QString str1,QString str2)
{
    //配置退出按钮
    MyPushButton * btn = new MyPushButton(str1,str2);
    btn->setParent(this);
    btn->setFixedSize(424,70);
    btn->move(650,650);
    connect(btn,&MyPushButton::clicked,[=](){
        this->close();
    });
}
void mainScene::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    pix1.load(":/ScreenShot/bgbgmid.jpg");
    pix2.load(":/ScreenShot/daylight.png");
    pix3.load(":/ScreenShot/yewan.png");
    switch (an) {
    case 0:
        painter.drawPixmap(0,0,this->width(),this->height(),pix2);
        break;
    case 1:
        painter.drawPixmap(0,0,this->width(),this->height(),pix1);
        break;
    default:
        painter.drawPixmap(0,0,this->width(),this->height(),pix3);
        break;
    }

}
