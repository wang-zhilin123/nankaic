#ifndef MAINSCENE_H
#define MAINSCENE_H
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QMainWindow>
#include<choosescene.h>
#include<QPainter>
#include<QAudioOutput>
#include <cstdlib> // 包含 rand() 和 srand()
#include <ctime>   // 包含 time()
QT_BEGIN_NAMESPACE
namespace Ui {
class mainScene;
}
QT_END_NAMESPACE

class mainScene : public QMainWindow
{
    Q_OBJECT

public:
    mainScene(QWidget *parent = nullptr);
    ~mainScene();
    int an;
    QMediaPlayer *player;
    QMediaPlayer * mu;
    QAudioOutput * audioOutput;
    QVideoWidget *videoWidget;
    chooseScene *choosescene;
    QPixmap pix1;
    QPixmap pix2;
    QPixmap pix3;
    void intScene();
    void videoPlay();
    void musicplay();
    void paintEvent(QPaintEvent *event);
    void buttonSet1(QString str1,QString str2="");
    void buttonSet2(QString str1,QString str2="");


private:
    Ui::mainScene *ui;
};
#endif // MAINSCENE_H
