#include "playtwo.h"
#include<QPainter>
#include<QMediaPlayer>
#include"gong.h"
#include"apple.h"
#include"bomb.h"
#include <QMouseEvent>
#include<QRect>
#include <cstdlib> // 包含 rand() 和 srand()
#include <ctime>   // 包含 time()
#include<QTimer>
#include<QDebug>
playtwo::playtwo(QWidget *parent)
    : QWidget{parent}
{
    Gong.pix.load(":/ScreenShot/taoc1.png");
    Gong.g_x =(1320-Gong.pix.width())*0.5;
    Gong.g_y =800-Gong.pix.height();
    //初始化矩形边框
    Gong.g_rect.setSize(QSize(Gong.pix.width(),Gong.pix.height()));
    Gong.g_rect.moveTo(Gong.g_x,Gong.g_y);
    for(int i = 0;i<30;i++)
    {
        Gong.g_apple[i].pix.load(":/ScreenShot/you75.png");
        Gong.g_apple[i].a_rect.setSize(QSize(Gong.g_apple[i].pix.width(),Gong.g_apple[i].pix.height()));
        Gong.g_apple[i].a_rect.moveTo(Gong.g_apple[i].a_x,Gong.g_apple[i].a_y);
    }
    Boss.pix.load(":/ScreenShot/fufu1.jpg");
    Boss.b_x = 1320+Boss.pix.width();
    Boss.b_y = 0;
    Boss.blood = 100;
    Boss.b_rect.setSize(QSize(Boss.pix.width(),Boss.pix.height()));
    Boss.b_rect.moveTo(Boss.b_x,Boss.b_y);
    for(int i=0;i<30;i++)
    {
        Boss.bgun[i].pix.load(":/ScreenShot/apple.png");
        Boss.bgun[i].ba_rect.setSize(QSize(Boss.bgun[i].pix.width(),Boss.bgun[i].pix.height()));
        Boss.bgun[i].ba_rect.moveTo(Boss.bgun[i].ba_x,Boss.bgun[i].ba_y);
    }
    intScene();//初始化场景
    timer1 = new QTimer(this);
    timer2 = new QTimer(this);
    playGame1();
    QTimer::singleShot(14000,this,[=](){
        playGame2();
    });
    QTimer::singleShot(20000,this,[=](){
        bossToScene(1320-Boss.pix.width());
    });

}
playtwo::~playtwo()
{

}
void playtwo::intScene()
{
    //固定窗口大小
    this->setFixedSize(1320,800);
    //设置窗口图标
    this->setWindowIcon(QIcon(":/ScreenShot/win!.jpg"));
    //设置窗口标题
    this->setWindowTitle("芙桃大战");

    //敌人出场时间间隔初始化
    p_recorder = 0;
    //随机数种子
    srand((unsigned int)time(NULL));
    //加载游戏结束图片
    gameOverlosefu.load(":/ScreenShot/fuwin.png");
    gameOverlosetao.load(":/ScreenShot/taolose.png");
    gameWinfu.load(":/ScreenShot/fule.png");
    gameWintao.load(":/ScreenShot/taole.png");
    gameOverlosefu = gameOverlosefu.scaled(500,500,Qt::KeepAspectRatio, Qt::SmoothTransformation);
    gameWintao = gameWintao.scaled(500,500,Qt::KeepAspectRatio, Qt::SmoothTransformation);
    gameOverlosetao = gameOverlosetao.scaled(500,500,Qt::KeepAspectRatio, Qt::SmoothTransformation);
}

void playtwo::bossToScene(int a)
{
    Boss.b_x = a;
    Boss.b_y = 0;
    Boss.b_rect.moveTo(Boss.b_x,Boss.b_y);
    Boss.b_free = false;
}
void playtwo::playGame1()
{
    //定时器1启动
    timer1->start(5);

    //监听定时器发送的信号
    connect(timer1,&QTimer::timeout,this,[=](){
        //更新游戏中所有元素的坐标
        updatePosition1();
        //绘制到屏幕中
        update();
        //检测碰撞
        colisionDetection();
    });

}

void playtwo::playGame2()
{
    //定时器2启动
    timer2->start(20);
    //监听
    connect(timer2,&QTimer::timeout,this,[=](){
        //敌人出现
        piaoToScene();
        //更新
        updatePosition2();
    });
}

void playtwo::updatePosition1()
{
    // 爆炸
    for(int i =0;i<=20;i++)
    {
        if(Bomb[i].m_free == false)
        {
            Bomb[i].updateInfo();
        }
    }


}

void playtwo::updatePosition2()
{
    if(Boss.b_free == false)
    {
        Boss.shoot();
        Boss.updatePosition();
    }
    //计算boss
    for(int i = 0;i<30;i++)
    {
        if(Boss.bgun[i].ba_free == false && Boss.b_free == false)
        {
            Boss.bgun[i].updateposition();
        }
    }


    //敌人出场
    for(int i = 0;i<50;i++)
    {
        if(Piao[i].p_free == false)
        {
            Piao[i].updatePosition();
        }
    }
    if(Gong.g_free == false)
    {
        //发射子弹
        Gong.shoot();
    }

    //计算gong所有非空闲子弹的当前坐标
    for(int i =0;i<30;i++)
    {
        //如果非空闲，计算发射位置
        if(Gong.g_apple[i].a_free == false &&Gong.g_free==false)
        {
            Gong.g_apple[i].updatePosition();
        }
    }
}

void playtwo::paintEvent(QPaintEvent *)
{
    //绘制地图
    QPainter painter(this);
    QPixmap pix1;
    pix1.load(":/ScreenShot/taodou.png");
    painter.drawPixmap(0,0,this->width(),this->height(),pix1);

    //绘制gong
    if(Gong.g_free == false)
    {
        painter.drawPixmap(Gong.g_x,Gong.g_y,Gong.pix);
    }
    if(Gong.g_free == true && Boss.b_free == false)
    {
        painter.drawPixmap(100,200,gameOverlosefu);
        painter.drawPixmap(720,200,gameOverlosetao);
    }
    if(fin == 1)
    {
        painter.drawPixmap(720,200,gameWintao);
        painter.drawPixmap(100,200,gameWinfu);
    }

    //绘制boss
    if(Boss.b_free == false && Gong.g_free == false)
    {
        painter.drawPixmap(Boss.b_x,Boss.b_y,Boss.pix);
        //绘制boss子弹
        for(int i = 0;i<30;i++)
        {
            if(Boss.bgun[i].ba_free == false)
            {
                painter.drawPixmap(Boss.bgun[i].ba_x,Boss.bgun[i].ba_y,Boss.bgun[i].pix);
            }
        }
    }

    //绘制苹果
    if(Gong.g_free == false)
    {
        for(int i = 0;i<30;i++)
        {
            if(Gong.g_apple[i].a_free == false)
            {
                painter.drawPixmap(Gong.g_apple[i].a_x,Gong.g_apple[i].a_y,Gong.g_apple[i].pix);
            }
        }
    }


    //绘制爆炸
    for(int i = 0;i<50;i++)
    {
        if(Bomb[i].m_free == false && Gong.g_free == false)
        {
            painter.drawPixmap(Bomb[i].m_x,Bomb[i].m_y,Bomb[i].m_pixarr[Bomb[i].m_index]);
        }
    }

    if(Gong.g_free == false)
    {
        //绘制飘浮灵
        for(int i=0;i<50;i++)
        {
            painter.drawPixmap(Piao[i].p_x,Piao[i].p_y,Piao[i].pix);
        }
    }

}

void playtwo::mouseMoveEvent(QMouseEvent *event)
{
    setMouseTracking(true);
    int x = event->x() - Gong.g_rect.width()/2;
    int y = event->y() - Gong.g_rect.height()/2;

    //边界检测
    if(x<=0)
    {
        x=0;
    }
    if(x>=1320 - Gong.g_rect.width())
    {
        x = 1320 - Gong.g_rect.width();
    }
    if(y<=0)
    {
        y = 0;
    }
    if(y>=800 - Gong.g_rect.height())
    {
        y = 800 - Gong.g_rect.height();
    }
    Gong.setPosition(x,y);
}

void playtwo::piaoToScene()
{
    p_recorder++;
    //未达到出场间隔，直接return
    if(p_recorder < 30)
    {
        return;
    }
    p_recorder = 0;
    for(int i = 0;i < 50;i++)
    {
        //空闲敌人出场
        if(Piao[i].p_free)
        {
            Piao[i].p_free = false;
            //坐标
            Piao[i].p_x = rand()%(800 - Piao[i].p_rect.width());
            Piao[i].p_y = 0;
            break;
        }

    }
}

void playtwo::colisionDetection()
{
    //遍历所有非空闲的敌人
    for(int i = 0;i<50;i++)
    {
        //空闲的敌人执行下一次循环
        if(Piao[i].p_free)
        {
            continue;
        }
        //芙芙被敌人肘击，坠机
        if(Piao[i].p_rect.intersects(Gong.g_rect))
        {
            Gong.g_free = true;
            for(int k = 0;k<50;k++)
            {
                if(Bomb[k].m_free)
                {
                    //播放爆炸音效
                    Bomb[k].m_free = false;
                    //更新爆炸坐标
                    Bomb[k].m_x = Gong.g_x;
                    Bomb[k].m_y = Gong.g_y;
                    break;
                }
            }
        }
        //遍历所有非空闲的苹果
        for(int j =0;j<30;j++)
        {
            if(Gong.g_apple[j].a_free)
            {
                continue;
            }
            //剩下非空闲的敌人和苹果
            if(Piao[i].p_rect.intersects(Gong.g_apple[j].a_rect))
            {
                Piao[i].p_free = true;
                Gong.g_apple[j].a_free = true;
                //播放爆炸效果
                for(int k = 0;k<50;k++)
                {
                    if(Bomb[k].m_free)
                    {
                        //播放爆炸音效
                        Bomb[k].m_free = false;
                        //更新爆炸坐标
                        Bomb[k].m_x = Piao[i].p_x;
                        Bomb[k].m_y = Piao[i].p_y;
                        break;
                    }
                }
            }
        }
    }
    for(int j = 0;j < 30;j++)
    {
        //qDebug()<<"1";
        if(Boss.bgun[j].ba_free)
        {
            continue;
        }
        //剩下非空闲的Boss子弹
        //qDebug()<<"2";
        if(Boss.bgun[j].ba_rect.intersects(Gong.g_rect))
        {
            Gong.g_free = true;
        }
    }
    for(int i =0;i<30;i++)
    {
        if(Gong.g_apple[i].a_free)
        {
            continue;
        }
        if(Gong.g_apple[i].a_rect.intersects(Boss.b_rect))
        {
            Gong.g_apple[i].a_free = true;
            Boss.blood -= 10;
            qDebug()<<Boss.blood;
            if(Boss.blood == 0)
            {
                fin = 1;
                qDebug()<<"over";
                Boss.b_free = true;
                Gong.g_free = true;
                qDebug()<<Boss.b_free;

            }
        }
    }
}
