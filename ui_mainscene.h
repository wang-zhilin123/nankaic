/********************************************************************************
** Form generated from reading UI file 'mainscene.ui'
**
** Created by: Qt User Interface Compiler version 6.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINSCENE_H
#define UI_MAINSCENE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mainScene
{
public:
    QWidget *centralwidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *mainScene)
    {
        if (mainScene->objectName().isEmpty())
            mainScene->setObjectName("mainScene");
        mainScene->resize(800, 600);
        centralwidget = new QWidget(mainScene);
        centralwidget->setObjectName("centralwidget");
        mainScene->setCentralWidget(centralwidget);
        menubar = new QMenuBar(mainScene);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 800, 25));
        mainScene->setMenuBar(menubar);
        statusbar = new QStatusBar(mainScene);
        statusbar->setObjectName("statusbar");
        mainScene->setStatusBar(statusbar);

        retranslateUi(mainScene);

        QMetaObject::connectSlotsByName(mainScene);
    } // setupUi

    void retranslateUi(QMainWindow *mainScene)
    {
        mainScene->setWindowTitle(QCoreApplication::translate("mainScene", "mainScene", nullptr));
    } // retranslateUi

};

namespace Ui {
    class mainScene: public Ui_mainScene {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINSCENE_H
